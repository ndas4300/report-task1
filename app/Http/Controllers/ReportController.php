<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ReportController extends Controller
{
    public function report(){
        $client = new Client();
        $url = "https://raw.githubusercontent.com/Bit-Code-Technologies/mockapi/main/purchase.json";
        $response = $client->request('GET', $url);

        $responseBody = json_decode($response->getBody());
        $data=collect($responseBody);

        foreach ($data as $d){
            $o=Order::where('order_no',$d->order_no)->exists();
            if(!$o){
                $c=Customer::where('name',$d->name)->first();
                if (!$c){
                    $customer=Customer::create([
                        'name'=>$d->name,
                        'phone'=>$d->user_phone,
                    ]);
                }
                $p=Product::where('name',$d->product_name)->first();
                if (!$p){
                    $product=Product::create([
                        'name'=>$d->product_name,
                        'code'=>$d->product_code,
                        'price'=>$d->product_price,
                    ]);
                }

                Order::create([
                    'order_no'=>$d->order_no,
                    'quantity'=>$d->purchase_quantity,
                    'customer_id'=>$c->id ?? $customer->id,
                    'product_id'=>$p->id ?? $product->id,
                    'total'=>$d->product_price * $d->purchase_quantity,
                    'date'=>Carbon::parse($d->created_at)->format('Y-m-d'),
                ]);
            }
        }

        $orders=Order::with(['customer','product'])->orderBy('total','DESC')->get();
        $grandTotal=$orders->sum('total');
        $totalQuan=$orders->sum('quantity');
        $totalPrice=$orders->sum('product.price');
        return view('welcome',compact('orders','grandTotal','totalQuan','totalPrice',));
    }
}
