<!<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="text-center m-5">
            <a href="{{route('report')}}" class="btn btn-primary">Generate Report</a>
        </div>

        @if(isset($orders))
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                    <th scope="col">Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key=>$order)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$order->product->name}}</td>
                        <td>{{$order->customer->name}}</td>
                        <td>{{$order->quantity}}</td>
                        <td>{{$order->product->price}}</td>
                        <td>{{$order->total}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: right">Gross Total</td>
                    <td >{{$totalQuan}}</td>
                    <td >{{$totalPrice}}</td>
                    <td >{{$grandTotal}}</td>
                </tr>

                </tbody>
            </table>
        @endif
    </div>

</div>

</body>
</html>
